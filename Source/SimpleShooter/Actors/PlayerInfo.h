// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlayerInfo.generated.h"

class UWidgetComponent;
class UPlayerInfoWidget;

UCLASS()
class SIMPLESHOOTER_API APlayerInfo : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlayerInfo();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SetCurrentHealth(const float CurrentHealth);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(Client, Reliable)
	void ClientInitPlayerInfo();

	UFUNCTION(Client, Reliable)
	void ClientFaceToOwningPawn();
	
private:
	UPROPERTY(EditDefaultsOnly, Category="PlayerInfo|Components")
	UWidgetComponent* PlayerInfoComp;

	UPlayerInfoWidget* PlayerInfoWidget;
};
