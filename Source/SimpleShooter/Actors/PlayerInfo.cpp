// Fill out your copyright notice in the Description page of Project Settings.
#include "PlayerInfo.h"
#include "Components/WidgetComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "SimpleShooter/Core/ShooterCharacter.h"
#include "SimpleShooter/Core/SimpleShooterPlayerState.h"
#include "SimpleShooter/UI/Gameplay/PlayerInfoWidget.h"

// Sets default values
APlayerInfo::APlayerInfo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PlayerInfoComp = CreateDefaultSubobject<UWidgetComponent>(TEXT("Player Info Component"));
	PlayerInfoComp->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APlayerInfo::BeginPlay()
{
	Super::BeginPlay();

	ClientInitPlayerInfo_Implementation();
}

void APlayerInfo::ClientInitPlayerInfo_Implementation()
{
	PlayerInfoWidget = Cast<UPlayerInfoWidget>(PlayerInfoComp->GetUserWidgetObject());
	
	if (auto Character = Cast<AShooterCharacter>(GetParentActor()))
	{
		Character->OnHealthChanged.AddDynamic(this, &APlayerInfo::SetCurrentHealth);

		auto FirstLocalController = GetGameInstance()->GetFirstLocalPlayerController();

		if (FirstLocalController && FirstLocalController == Character->GetController())
		{
			SetActorHiddenInGame(true);
		}
		else if (PlayerInfoWidget)
		{
			FString PlayerName = TEXT("DefaultName");
			if (auto PlayerState = Cast<ASimpleShooterPlayerState>(Character->GetPlayerState()))
				PlayerName = PlayerState->GetPlayerNameCustom();

			PlayerInfoWidget->InitializeWidget(
					PlayerName,
					Character->GetCurrentHealth(),
					Character->GetMaxHealth());
		}
	}
}

void APlayerInfo::ClientFaceToOwningPawn_Implementation()
{
	auto FirstLocalController = GetGameInstance()->GetFirstLocalPlayerController();
	
	if (GetParentActor() && FirstLocalController && FirstLocalController->GetPawn())
	{
		FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(
			GetParentActor()->GetActorLocation(),
			FirstLocalController->GetPawn()->GetActorLocation());
		LookAtRotation.Pitch = 0.f;
		LookAtRotation.Roll = 0.f;
		PlayerInfoComp->SetWorldRotation(LookAtRotation);
	}
}

// Called every frame
void APlayerInfo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ClientFaceToOwningPawn_Implementation();
}

void APlayerInfo::SetCurrentHealth(const float CurrentHealth)
{
	if (CurrentHealth <= 0.f)
		Destroy();
	if (PlayerInfoWidget)
		PlayerInfoWidget->SetHealth(CurrentHealth);
}

