// Fill out your copyright notice in the Description page of Project Settings.
#include "MainMenu.h"

#include "MultiplayerSetupScreen.h"
#include "SettingsScreen.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

void UMainMenu::NativeConstruct()
{
	Super::NativeConstruct();

	SinglePlayerBttn->OnReleased.AddDynamic(this, &UMainMenu::OpenLobby);
	MultiPlayerBttn->OnReleased.AddDynamic(this, &UMainMenu::ShowMultiplayerSetup);
	SettingsBttn->OnReleased.AddDynamic(this, &UMainMenu::ShowSettings);
	ExitBttn->OnReleased.AddDynamic(this, &UMainMenu::QuitGame);
}

void UMainMenu::OpenLobby()
{
	UGameplayStatics::OpenLevel(GetWorld(), TEXT("Lobby_Master"));
	//UGameplayStatics::OpenLevelBySoftObjectPtr(GetWorld(), );
}

void UMainMenu::ShowMultiplayerSetup()
{
	WidgetSwitcher->SetActiveWidget(MultiplayerSetupWidget);
	MultiplayerSetupWidget->SetVisibility(ESlateVisibility::Visible);
}

void UMainMenu::ShowSettings()
{
	WidgetSwitcher->SetActiveWidget(SettingsWidget);
	SettingsWidget->SetVisibility(ESlateVisibility::Visible);
}

void UMainMenu::QuitGame()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), GetOwningPlayer(), EQuitPreference::Quit, true);
}
