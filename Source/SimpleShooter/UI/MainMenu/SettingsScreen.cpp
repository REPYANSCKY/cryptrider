// Fill out your copyright notice in the Description page of Project Settings.
#include "SettingsScreen.h"
#include "Components/Button.h"
#include "Components/ComboBoxString.h"
#include "Components/EditableTextBox.h"
#include "Components/WidgetSwitcher.h"
#include "Engine/DataTable.h"
#include "GameFramework/GameUserSettings.h"
#include "SimpleShooter/Core/SimpleShooterPlayerState.h"

void USettingsScreen::NativeConstruct()
{
	Super::NativeConstruct();

	SetVisibility(ESlateVisibility::Hidden);
	SetDefaultGameInfo();
	
	ApplyBttn->OnReleased.AddDynamic(this, &USettingsScreen::ApplySettings);
	DiscardBttn->OnReleased.AddDynamic(this, &USettingsScreen::DiscardSettings);
}

void USettingsScreen::SetDefaultGameInfo()
{
	ScreenModeSlctnBox->ClearOptions();
	
	for (FName RowName : ScreenOptions->GetRowNames())
	{
		auto ScreenMode =  ScreenOptions->FindRow<FScreenMode>(RowName, "");

		if (ScreenMode)
		{
			ScreenModeSlctnBox->AddOption(ScreenMode->TypeName.ToString());
		
			if (UGameUserSettings::GetGameUserSettings()->GetFullscreenMode() == ScreenMode->Type.GetValue())
				ScreenModeSlctnBox->SetSelectedOption(ScreenMode->TypeName.ToString());
		}
	}

	if (auto PlayerState = Cast<ASimpleShooterPlayerState>(GetOwningPlayerPawn()->GetPlayerState()))
	{
		PlayerNameEditTxt->SetText(FText::FromString(PlayerState->GetPlayerName()));
	}
}

void USettingsScreen::ApplySettings()
{
	for (FName RowName : ScreenOptions->GetRowNames())
	{
		FScreenMode* ScreenMode =  ScreenOptions->FindRow<FScreenMode>(RowName, TEXT(""));
		if (ScreenMode && ScreenModeSlctnBox->GetSelectedOption() == ScreenMode->TypeName.ToString())  
			if (UGameUserSettings::GetGameUserSettings()->GetFullscreenMode() == ScreenMode->Type.GetValue())
			{
				UGameUserSettings::GetGameUserSettings()->SetFullscreenMode(ScreenMode->Type.GetValue());
				UGameUserSettings::GetGameUserSettings()->ApplyResolutionSettings(true);
				UGameUserSettings::GetGameUserSettings()->ConfirmVideoMode();
				continue;
		}
	}

	if (auto PlayerState = Cast<ASimpleShooterPlayerState>(GetOwningPlayerPawn()->GetPlayerState()))
	{
		PlayerState->SetPlayerNameCustom(PlayerNameEditTxt->GetText().ToString());
	}

	CloseSettingsTab();
}

void USettingsScreen::DiscardSettings()
{
	SetDefaultGameInfo();
	CloseSettingsTab();
}

void USettingsScreen::CloseSettingsTab()
{
	SetVisibility(ESlateVisibility::Hidden);
}
