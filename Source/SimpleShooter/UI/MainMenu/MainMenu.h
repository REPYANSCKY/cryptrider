// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenu.generated.h"

class UWidgetSwitcher;
class UImage;
class UTextBlock;
class UButton;
class USettingsScreen;
class UMultiplayerSetupScreen;

UCLASS()
class SIMPLESHOOTER_API UMainMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

protected:
	UFUNCTION()
	void OpenLobby();

	UFUNCTION()
	void ShowMultiplayerSetup();

	UFUNCTION()
	void ShowSettings();

	UFUNCTION()
	void QuitGame();

private:
	UPROPERTY(EditDefaultsOnly, Category="MainMenu|Appearence", meta=(BindWidget))
	UTextBlock* HeaderTitleTxt;
	
	UPROPERTY(EditDefaultsOnly, Category="MainMenu|Appearence", meta=(BindWidget))
	UWidgetSwitcher* WidgetSwitcher;

	UPROPERTY(EditDefaultsOnly, Category="MainMenu|Buttons", meta=(BindWidget))
	UButton* SinglePlayerBttn;

	UPROPERTY(EditDefaultsOnly, Category="MainMenu|Buttons", meta=(BindWidget))
	UButton* MultiPlayerBttn;

	UPROPERTY(EditDefaultsOnly, Category="MainMenu|Buttons", meta=(BindWidget))
	UButton* SettingsBttn;

	UPROPERTY(EditDefaultsOnly, Category="MainMenu|Buttons", meta=(BindWidget))
	UButton* ExitBttn;

	UPROPERTY(EditDefaultsOnly, Category="MainMenu|Widgets", meta=(BindWidget))
	USettingsScreen* SettingsWidget;

	UPROPERTY(EditDefaultsOnly, Category="MainMenu|Widgets", meta=(BindWidget))
	UMultiplayerSetupScreen* MultiplayerSetupWidget;
};
