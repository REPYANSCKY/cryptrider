// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MultiplayerSetupScreen.generated.h"

class UHostGameScreen;
class UWidgetSwitcher;
class UScrollBox;
class UButton;

UCLASS()
class SIMPLESHOOTER_API UMultiplayerSetupScreen : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

protected:
	void FindSessions();

	UFUNCTION()
	void OnSessionsFound(bool bSuccessful);
	
	UFUNCTION()
	void OpenSessionCreation();

	UFUNCTION()
	void CloseMultiplayerTab();

private:
	UPROPERTY(EditDefaultsOnly, Category="MultiplayerSetupScreen|Appearance", meta=(BindWidget))
	UWidgetSwitcher* WidgetSwitcher;

	UPROPERTY(EditDefaultsOnly, Category="MultiplayerSetupScreen|Appearance", meta=(BindWidget))
	UHostGameScreen* HostGameWidget;
	
	UPROPERTY(EditDefaultsOnly, Category="MultiplayerSetupScreen|Buttons", meta=(BindWidget))
	UScrollBox* SessionsList;
	
	UPROPERTY(EditDefaultsOnly, Category="MultiplayerSetupScreen|Buttons", meta=(BindWidget))
	UButton* HostGameBttn;
	
	UPROPERTY(EditDefaultsOnly, Category="MultiplayerSetupScreen|Buttons", meta=(BindWidget))
	UButton* BackBttn;
};
