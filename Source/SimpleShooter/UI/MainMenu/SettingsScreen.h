// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Engine/DataTable.h"
#include "SettingsScreen.generated.h"

USTRUCT(BlueprintType, meta = (DisplayName = "ScreenMode"))
struct FScreenMode : public FTableRowBase 
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, SimpleDisplay)
	TEnumAsByte<EWindowMode::Type> Type;

	UPROPERTY(EditAnywhere, SimpleDisplay)
	FText TypeName;
};

class UDataTable;
class UComboBoxString;
class UButton;
class UEditableTextBox;

UCLASS()
class SIMPLESHOOTER_API USettingsScreen : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

protected:
	UFUNCTION()
	void SetDefaultGameInfo();

	UFUNCTION()
	void ApplySettings();

	UFUNCTION()
	void DiscardSettings();

	void CloseSettingsTab();

public:
	UPROPERTY(EditAnywhere, Category="SettingsScreen|Settings")
	UDataTable* ScreenOptions;

private:
	UPROPERTY(EditDefaultsOnly, Category="SettingsScreen|Settings", meta=(BindWidget))
	UComboBoxString* ScreenModeSlctnBox;
	
	UPROPERTY(EditDefaultsOnly, Category="SettingsScreen|Settings", meta=(BindWidget))
	UEditableTextBox* PlayerNameEditTxt;

	UPROPERTY(EditDefaultsOnly, Category="SettingsScreen|Buttons", meta=(BindWidget))
	UButton* ApplyBttn;

	UPROPERTY(EditDefaultsOnly, Category="SettingsScreen|Buttons", meta=(BindWidget))
	UButton* DiscardBttn;
};
