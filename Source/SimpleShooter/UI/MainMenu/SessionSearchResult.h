// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SessionSearchResult.generated.h"

UCLASS()
class SIMPLESHOOTER_API USessionSearchResult : public UUserWidget
{
	GENERATED_BODY()
	
public:
	USessionSearchResult(FOnlineSessionSearchResult* SearchResult);

private:
	class FOnlineSessionSearchResult* SessionSearchResult;

	 
};
