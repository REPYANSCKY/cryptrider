// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HostGameScreen.generated.h"

class UCheckBox;
class UButton;
class UEditableTextBox;

UCLASS()
class SIMPLESHOOTER_API UHostGameScreen : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

protected:
	UFUNCTION()
	void CreateSession();

	UFUNCTION()
	void ReturnToSessions();

private:
	UPROPERTY(EditDefaultsOnly, Category="HostGameScreen|Settings", meta=(BindWidget))
	UEditableTextBox* ServerNameEditTxt;

	UPROPERTY(EditDefaultsOnly, Category="HostGameScreen|Settings", meta=(BindWidget))
	UEditableTextBox* PlayersNumberEditTxt;

	UPROPERTY(EditDefaultsOnly, Category="HostGameScreen|Settings", meta=(BindWidget))
	UCheckBox* ConnectionTypeChck;

	UPROPERTY(EditDefaultsOnly, Category="HostGameScreen|Buttons", meta=(BindWidget))
	UButton* CreateSessionBttn;

	UPROPERTY(EditDefaultsOnly, Category="HostGameScreen|Buttons", meta=(BindWidget))
	UButton* ReturnBttn;
};
