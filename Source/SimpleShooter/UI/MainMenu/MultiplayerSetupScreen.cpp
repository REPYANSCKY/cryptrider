// Fill out your copyright notice in the Description page of Project Settings.
#include "MultiplayerSetupScreen.h"
#include "HostGameScreen.h"
#include "OnlineSessionSettings.h"
#include "Components/Button.h"
#include "Components/ScrollBox.h"
#include "Components/WidgetSwitcher.h"
#include "SimpleShooter/Core/ShooterGameInstance.h"

void UMultiplayerSetupScreen::NativeConstruct()
{
	Super::NativeConstruct();

	SetVisibility(ESlateVisibility::Hidden);
	
	BackBttn->OnReleased.AddDynamic(this, &UMultiplayerSetupScreen::CloseMultiplayerTab);
	HostGameBttn->OnReleased.AddDynamic(this, &UMultiplayerSetupScreen::OpenSessionCreation);

	
}

void UMultiplayerSetupScreen::FindSessions()
{
	if (UShooterGameInstance* GameInstance = GetGameInstance<UShooterGameInstance>())
	{
		GameInstance->OnFindSessionsCompleteDelegate.CreateUObject(this, &UMultiplayerSetupScreen::OnSessionsFound);
		
		if (ULocalPlayer* Player = GameInstance->GetFirstGamePlayer())
			GameInstance->FindSessions(Player->GetPreferredUniqueNetId().GetUniqueNetId(), true, true);
	}
}

void UMultiplayerSetupScreen::OnSessionsFound(bool bSuccessful)
{
	if (bSuccessful)
	{
		if (UShooterGameInstance* GameInstance = GetGameInstance<UShooterGameInstance>())
		{
			for (auto SearchResult : GameInstance->SessionSearch->SearchResults)
			{
				int32 Num = SearchResult.Session.SessionSettings.NumPublicConnections - SearchResult.Session.NumOpenPublicConnections;
				//SessionsList->AddChild();
			}
		}
	}
	else
	{
		// TODO: Add failure message
	}
}

void UMultiplayerSetupScreen::OpenSessionCreation()
{
	WidgetSwitcher->SetActiveWidget(HostGameWidget);
}

void UMultiplayerSetupScreen::CloseMultiplayerTab()
{
	SetVisibility(ESlateVisibility::Hidden);
}
