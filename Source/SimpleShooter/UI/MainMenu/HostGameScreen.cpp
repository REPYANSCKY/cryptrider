// Fill out your copyright notice in the Description page of Project Settings.
#include "HostGameScreen.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"

void UHostGameScreen::NativeConstruct()
{
	Super::NativeConstruct();

	CreateSessionBttn->OnReleased.AddDynamic(this, &UHostGameScreen::CreateSession);
	ReturnBttn->OnReleased.AddDynamic(this, &UHostGameScreen::ReturnToSessions);
}

void UHostGameScreen::CreateSession()
{
	// TODO: Session creation
}

void UHostGameScreen::ReturnToSessions()
{
	UWidgetSwitcher* WidgetSwitcher = Cast<UWidgetSwitcher>(GetParent());
	if (WidgetSwitcher)
	{
		int NewActiveWidgetIndex = WidgetSwitcher->GetActiveWidgetIndex() - 1;
		if (NewActiveWidgetIndex >= 0)
			WidgetSwitcher->SetActiveWidgetIndex(NewActiveWidgetIndex);
	}
}
