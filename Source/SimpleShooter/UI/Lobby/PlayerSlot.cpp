// Fill out your copyright notice in the Description page of Project Settings.
#include "PlayerSlot.h"
#include "Components/Button.h"

void UPlayerSlot::NativeConstruct()
{
	Super::NativeConstruct();

	BindEvents();
}

void UPlayerSlot::BindEvents()
{
	SlotBttn->OnReleased.AddDynamic(this, &UPlayerSlot::SlotPressed);
}

void UPlayerSlot::SlotPressed()
{
	
}
