// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerSlot.generated.h"

UENUM(BlueprintType)
enum class EPlayerSlotState : uint8
{
	NotOccupied = 0,
	Player,
	Bot
};

class UButton;
class UTextBlock;

UCLASS()
class SIMPLESHOOTER_API UPlayerSlot : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

	virtual void BindEvents();

protected:
	UFUNCTION()
	void SlotPressed();

private:
	UPROPERTY(EditDefaultsOnly, Category="PlayerSlot|Buttons", meta=(BindWidget))
	UButton* SlotBttn;

	UPROPERTY(EditDefaultsOnly, Category="PlayerSlot|Appearance", meta=(BindWidget))
	UTextBlock* ButtonTxt;
	
	EPlayerSlotState SlotState = EPlayerSlotState::NotOccupied;
};
