// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerInfoWidget.generated.h"

class UTextBlock;
class UHealthIndicator;

UCLASS()
class SIMPLESHOOTER_API UPlayerInfoWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void InitializeWidget(const FString& PlayerName, float CurrHealth, float MaxHealth);
	
	void SetHealth(float CurrentHealth);

private:
	UPROPERTY(EditDefaultsOnly, Category="PlayerInfo|Appearance", meta=(BindWidget))
	UHealthIndicator* HealthIndicatorWidget;

	UPROPERTY(EditDefaultsOnly, Category="PlayerInfo|Appearance", meta=(BindWidget))
	UTextBlock* PlayerNameTxt;
};
