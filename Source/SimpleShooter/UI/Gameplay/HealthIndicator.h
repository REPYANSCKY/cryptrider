// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HealthIndicator.generated.h"

class UTextBlock;
class UProgressBar;

UCLASS()
class SIMPLESHOOTER_API UHealthIndicator : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetHealth(float CurrentHealth);

	void SetMaxHealth(float Health);

private:
	UPROPERTY(EditDefaultsOnly, Category="HealthBar|Appearance", meta=(BindWidget))
	UProgressBar* HealthBar;

	UPROPERTY(EditDefaultsOnly, Category="HealthBar|Appearance", meta=(BindWidget))
	UTextBlock* HealthInfoTxt;
	
	float MaxHealth = 0.f;
};
