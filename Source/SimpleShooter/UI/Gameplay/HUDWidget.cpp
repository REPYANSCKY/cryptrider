// Fill out your copyright notice in the Description page of Project Settings.
#include "HUDWidget.h"
#include "HealthIndicator.h"

void UHUDWidget::InitHUDWidget(const float CurrHealth, const float MaxHealth)
{
	HealthIndicatorWidget->SetMaxHealth(MaxHealth);
	HealthIndicatorWidget->SetHealth(CurrHealth);
}

void UHUDWidget::SetCurrentHealth(const float CurrentHealth)
{
	HealthIndicatorWidget->SetHealth(CurrentHealth);
}
