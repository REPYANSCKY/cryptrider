// Fill out your copyright notice in the Description page of Project Settings.
#include "HealthIndicator.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"

void UHealthIndicator::SetHealth(float CurrentHealth)
{
	HealthInfoTxt->SetText(FText::AsNumber((int)CurrentHealth));
	HealthBar->SetPercent(CurrentHealth/MaxHealth);
	GEngine->AddOnScreenDebugMessage(0, 5.f, FColor::Yellow, FString::FromInt(CurrentHealth));
}

void UHealthIndicator::SetMaxHealth(float Health)
{
	MaxHealth = Health;
}
