// Fill out your copyright notice in the Description page of Project Settings.
#include "PlayerInfoWidget.h"
#include "HealthIndicator.h"
#include "Components/TextBlock.h"

void UPlayerInfoWidget::InitializeWidget(const FString& PlayerName, float CurrHealth, float MaxHealth)
{
	PlayerNameTxt->SetText(FText::FromString(PlayerName));
	HealthIndicatorWidget->SetMaxHealth(MaxHealth);
	HealthIndicatorWidget->SetHealth(CurrHealth);
}

void UPlayerInfoWidget::SetHealth(float CurrentHealth)
{
	HealthIndicatorWidget->SetHealth(CurrentHealth);
}
