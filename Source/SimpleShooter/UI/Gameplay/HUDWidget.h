// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HUDWidget.generated.h"

class UImage;
class UHealthIndicator;

UCLASS()
class SIMPLESHOOTER_API UHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void InitHUDWidget(const float CurrHealth, const float MaxHealth);
	
	UFUNCTION()
	void SetCurrentHealth(const float CurrentHealth);

private:
	UPROPERTY(EditDefaultsOnly, Category="HUDWidget|Appearance", meta=(BindWidget))
	UImage* AimMarkerImg;
	
	UPROPERTY(EditDefaultsOnly, Category="HUDWidget|Appearance", meta=(BindWidget))
	UHealthIndicator* HealthIndicatorWidget;
};
