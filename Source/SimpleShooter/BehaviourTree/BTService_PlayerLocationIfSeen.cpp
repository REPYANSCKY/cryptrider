// Fill out your copyright notice in the Description page of Project Settings.
#include "BTService_PlayerLocationIfSeen.h"
#include "AIController.h"
#include "EngineUtils.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTService_PlayerLocationIfSeen::UBTService_PlayerLocationIfSeen()
{
	NodeName = TEXT("Update Player Location If Seen");
}

void UBTService_PlayerLocationIfSeen::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	for (auto Pawn : TActorRange<APawn>(GetWorld()))
	{
		if (OwnerComp.GetAIOwner() && Pawn)
		{
			if(OwnerComp.GetAIOwner()->LineOfSightTo(Pawn))
			{
				OwnerComp.GetBlackboardComponent()->SetValueAsObject(GetSelectedBlackboardKey(), Pawn);
			}
			else
				OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
		}
	}
}

bool UBTService_PlayerLocationIfSeen::IsEnemyInTheViewAngle(APlayerController* Controller, APawn* EnemyPawn)
{
	FVector EyesLocation;
	FRotator EyesRotation;
	Controller->GetPawn()->GetActorEyesViewPoint(EyesLocation, EyesRotation);

	return true;
}
