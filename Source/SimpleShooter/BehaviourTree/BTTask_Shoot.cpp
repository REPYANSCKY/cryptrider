// Fill out your copyright notice in the Description page of Project Settings.
#include "BTTask_Shoot.h"
#include "AIController.h"
#include "SimpleShooter/Core/ShooterCharacter.h"

UBTTask_Shoot::UBTTask_Shoot()
{
	NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type Result = EBTNodeResult::Failed;
	Super::ExecuteTask(OwnerComp, NodeMemory);
	
	if (auto Owner = OwnerComp.GetAIOwner())
	{
		if (auto Character = Cast<AShooterCharacter>(Owner->GetPawn()))
		{
			if (!Character->IsDead())
			{
				Character->ServerShoot();
				Result = EBTNodeResult::Succeeded;
			}
			else
				Result = EBTNodeResult::Aborted;
		}
	}
	
	return Result;
}
