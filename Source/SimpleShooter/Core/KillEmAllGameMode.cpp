// Fill out your copyright notice in the Description page of Project Settings.
#include "KillEmAllGameMode.h"
#include "EngineUtils.h"
#include "ShooterAIController.h"
#include "ShooterPlayerController.h"

void AKillEmAllGameMode::Server_PawnKilled_Implementation(APawn* PawnKilled)
{
	for (AShooterPlayerController* Controller : TActorRange<AShooterPlayerController>(GetWorld()))
	{
		if (!Controller->IsDead()) return;
	}

	// For loop over ShooterAI in world:
	for (AShooterAIController* Controller : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (!Controller->IsDead()) return;
	}

	// End game
	Server_EndGame_Implementation(0);
}

void AKillEmAllGameMode::Server_EndGame_Implementation(int WinnerTeamIndex)
{
	for (AController* Controller : TActorRange<AController>(GetWorld()))
	{
		Controller->GameHasEnded(Controller->GetPawn(), Controller->IsPlayerController() == (bool)WinnerTeamIndex);
	}
}
