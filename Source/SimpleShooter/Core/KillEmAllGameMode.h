// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SimpleShooterGameModeBase.h"
#include "KillEmAllGameMode.generated.h"

UCLASS()
class SIMPLESHOOTER_API AKillEmAllGameMode : public ASimpleShooterGameModeBase
{
	GENERATED_BODY()

public:
	virtual void Server_PawnKilled_Implementation(APawn* PawnKilled) override;

private:
	virtual void Server_EndGame_Implementation(int WinnerTeamIndex) override;
};
