// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.generated.h"

UCLASS()
class SIMPLESHOOTER_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void ClientGameEnded_Implementation(AActor* EndGameFocus, bool bIsWinner) override;
	
	bool IsDead();

protected:
	virtual void BeginPlay() override;

private:
	UFUNCTION(Client, Reliable)
	virtual void ClientInitHUD(TSubclassOf<UUserWidget> HUDWidget);
	
private:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> MatchResultClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> HUDClass;
	
	UPROPERTY(EditDefaultsOnly)
	float RestartDelay = 5.f;

	FTimerHandle RestartTimer;

	UPROPERTY(VisibleDefaultsOnly)
	UUserWidget* HUD;
};
