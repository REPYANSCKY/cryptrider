// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleShooterPlayerState.h"

ASimpleShooterPlayerState::ASimpleShooterPlayerState()
{
	bUseCustomPlayerNames = true;
}

FString ASimpleShooterPlayerState::GetPlayerNameCustom() const
{
	return PlayerName;
}

void ASimpleShooterPlayerState::SetPlayerNameCustom(const FString& S)
{
	PlayerName = S;
}
