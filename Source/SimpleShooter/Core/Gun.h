// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Gun.generated.h"

UCLASS()
class SIMPLESHOOTER_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void PullTrigger();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USkeletalMeshComponent* MeshComponent;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastSpawnEffectsAttached(
		USceneComponent* AttachToComponent,
		FName AttachPointName,
		UParticleSystem* Particle = nullptr,
		USoundBase* Sound = nullptr);
	
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSpawnEffectsAtLocation(
		FVector Location = FVector(),
		FRotator Rotation  = FRotator(),
		UParticleSystem* Particle = nullptr,
		USoundBase* Sound = nullptr);

	AController* GetOwnerController() const;

private:
	UPROPERTY(VisibleAnywhere)
	class USceneComponent* SceneComponent;

	/*UPROPERTY(EditAnywhere)
	class USkeletalMeshComponent* MeshComponent;*/

	UPROPERTY(EditAnywhere)
	UParticleSystem* MuzzleFlesh;

	UPROPERTY(EditAnywhere)
	USoundBase* MuzzleSound;
	
	UPROPERTY(EditAnywhere)
	UParticleSystem* HitFlesh;

	UPROPERTY(EditAnywhere)
	USoundBase* HitSound;
	
	UPROPERTY(EditAnywhere)
	float MaxRange = 10000.f;
	
	UPROPERTY(EditAnywhere)
	float Damage = 30.f;
};
