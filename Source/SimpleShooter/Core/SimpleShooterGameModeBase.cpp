// Copyright Epic Games, Inc. All Rights Reserved.
#include "SimpleShooterGameModeBase.h"

void ASimpleShooterGameModeBase::Server_PawnKilled_Implementation(APawn* PawnKilled)
{
}

void ASimpleShooterGameModeBase::Server_EndGame_Implementation(int WinnerTeamIndex)
{
}
