// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChanged, const float, CurrentHealth);

class APlayerInfo;
class AGun;

UCLASS()
class SIMPLESHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	UFUNCTION(NetMulticast, Reliable)
	virtual void MulticastReceivedDamage(const float DamageAmount);

	UFUNCTION(BlueprintPure)
	bool IsDead() const { return CurrentHealth <= 0.f; }

	UFUNCTION(BlueprintPure)
	float GetHealthPercent() const;

	UFUNCTION(BlueprintPure)
	float GetCurrentHealth() const { return CurrentHealth; }

	UFUNCTION(BlueprintPure)
	float GetMaxHealth() const { return MaxHealth; }
	
	UFUNCTION(Server, Unreliable)
	void ServerShoot();

public:
	uint8 TeamId = 0;
	
	AGun* Gun;
	
	FOnHealthChanged OnHealthChanged;

private:
	UPROPERTY(EditAnywhere)
	class USpringArmComponent* SpringArm;
	
	UPROPERTY(EditAnywhere)
	class UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float RotationRate = 10.f;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float SpeedRate =  0.5;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGun> GunClass;

	UPROPERTY(EditDefaultsOnly)
	UChildActorComponent* PlayerInfoComp;

	UPROPERTY(EditDefaultsOnly)
	float MaxHealth = 100.f;

	UPROPERTY(VisibleAnywhere, Replicated)
	float CurrentHealth = 100.f;

private:
	void MoveForward(float AxisValue);
	
	void MoveRight(float AxisValue);
	
	void LookUpRate(float AxisValue);
	
	void LookRightRate(float AxisValue);
	
	void AccelerationPressed();
	
	void AccelerationReleased();
};
