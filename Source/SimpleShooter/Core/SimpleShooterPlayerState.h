// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SimpleShooterPlayerState.generated.h"

UCLASS()
class SIMPLESHOOTER_API ASimpleShooterPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	ASimpleShooterPlayerState();

	virtual FString GetPlayerNameCustom() const override;

	void SetPlayerNameCustom(const FString& S);

	virtual void SetTeamId(const uint8 NewTeamId) { TeamId = NewTeamId; }

	virtual uint8 GetTeamId() { return TeamId; }

private:
	uint8 TeamId = 0;
	
	FString PlayerName = TEXT("Player");
};
