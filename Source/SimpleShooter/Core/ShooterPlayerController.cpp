// Fill out your copyright notice in the Description page of Project Settings.
#include "ShooterPlayerController.h"
#include "ShooterCharacter.h"
#include "Blueprint/UserWidget.h"
#include "SimpleShooter/UI/Gameplay/HUDWidget.h"

void AShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	ClientInitHUD_Implementation(HUDClass);
}

void AShooterPlayerController::ClientInitHUD_Implementation(TSubclassOf<UUserWidget> HUDWidget)
{
	if (HUD != nullptr)
		HUD->RemoveFromParent();
	
	HUD = CreateWidget(this, HUDWidget);
	if (HUD)
	{
		HUD->AddToViewport();

		AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(GetPawn());
		UHUDWidget* tempWidget = Cast<UHUDWidget>(HUD);
		if (ShooterCharacter && tempWidget)
		{
			tempWidget->InitHUDWidget(ShooterCharacter->GetCurrentHealth(), ShooterCharacter->GetMaxHealth());
			ShooterCharacter->OnHealthChanged.AddDynamic(tempWidget, &UHUDWidget::SetCurrentHealth);
		}
	}
}

void AShooterPlayerController::ClientGameEnded_Implementation(AActor* EndGameFocus, bool bIsWinner)
{
	Super::ClientGameEnded_Implementation(EndGameFocus, bIsWinner);

	ClientInitHUD_Implementation(MatchResultClass);
}

bool AShooterPlayerController::IsDead()
{
	AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(GetPawn());
	if (ShooterCharacter)
	{
		return ShooterCharacter->IsDead();
	}

	return true;
}
