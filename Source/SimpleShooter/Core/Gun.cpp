// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Engine/DamageEvents.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"

// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	this->SetReplicates(true);

	SceneComponent = CreateDefaultSubobject<USceneComponent>("Root");
	SetRootComponent(SceneComponent);

	MeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>("Skeletal");
	MeshComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection)
{
	bool bIsHit = false;

	if (auto Controller = GetOwnerController())
	{
		FVector ViewLocation;
		FRotator ViewRotation;
		Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);

		FVector End = ViewLocation + ViewRotation.Vector() * MaxRange;

		FCollisionQueryParams Params;
		Params.AddIgnoredActor(this);
		Params.AddIgnoredActor(GetOwner());
		Params.bTraceComplex = true;

		bIsHit = GetWorld()->LineTraceSingleByChannel(
			Hit,
			ViewLocation,
			End,
			ECC_GameTraceChannel1,
			Params
		);

		ShotDirection = -ViewRotation.Vector();
	}

	return  bIsHit;
}

AController* AGun::GetOwnerController() const
{
	AController* Controller = nullptr;

	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn)
	{
		if (OwnerPawn->GetController())
		{
			Controller = OwnerPawn->GetController();
		}
	}

	return Controller;
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGun::PullTrigger()
{
	MulticastSpawnEffectsAttached(MeshComponent, TEXT("MuzzleFlashSocket"), MuzzleFlesh, MuzzleSound);
	
	FHitResult HitResult;
	FVector ShotDirection;
	
	if (GunTrace(HitResult, ShotDirection))
	{
		MulticastSpawnEffectsAtLocation(HitResult.ImpactPoint, ShotDirection.Rotation(), MuzzleFlesh, MuzzleSound);
	
		AActor* HitActor = HitResult.GetActor();
		if (HitActor)
		{
			float CalculatedDamage = FMath::RandRange(Damage / 2, Damage) * ((MaxRange - HitResult.Distance) / MaxRange);  
	
			FPointDamageEvent DamageEvent(
				CalculatedDamage,
				HitResult,
				ShotDirection,
				nullptr
			);
	
			if (auto Controller = GetOwnerController())
			{
				HitActor->TakeDamage(
					 CalculatedDamage,
					 DamageEvent,
					 Controller,
					 this
				 );
			}
		}
	}
}

void AGun::MulticastSpawnEffectsAttached_Implementation(
	USceneComponent* AttachToComponent,
	FName AttachPointName,
	UParticleSystem* Particle,
	USoundBase* Sound)
{
	if (Particle->IsValidLowLevel())
		UGameplayStatics::SpawnEmitterAttached(Particle, AttachToComponent, AttachPointName);

	if (Sound->IsValidLowLevel())
		UGameplayStatics::SpawnSoundAttached(Sound, AttachToComponent, AttachPointName);
}

void AGun::MulticastSpawnEffectsAtLocation_Implementation(
	FVector Location,
	FRotator Rotation,
	UParticleSystem* Particle,
	USoundBase* Sound)
{
	if (Particle->IsValidLowLevel())
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), Particle, Location, Rotation);

	if (Sound->IsValidLowLevel())
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, Location);
}
